/*
 * Copyright (C) 2020 Jonsui
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.monmo.java.app;

import org.monmo.java.entities.ListaCreden;
import org.monmo.java.entities.ListaLog;
import org.monmo.java.entities.Login;
import org.monmo.java.entities.Logout;
import org.monmo.java.entities.Rol;
import org.monmo.java.entities.User;
import org.monmo.java.libraries.UtilesEntrada;
import org.monmo.java.libraries.UtilesPassword;
import org.monmo.java.libraries.UtilesValidacion;
import org.monmo.java.menuitems.app.AppItems;

/**
 *
 * @author Jonsui
 */
public class App {

  public static final String MSG_OP = "Introduzca una opción: ";
  public static final String MSG_USER = "Nombre de usuario (0 para salir).: ";
  public static final String MSG_NEW_USER = "Nombre del nuevo usuario .: ";
  public static final String MSG_NEW_PASS = "Escriba nuevo password ...: ";
  public static final String MSG_NEW_ROL = "Designe nuevo rol ........: ";
  public static final String MSG_PASS = "Password ..........: ";
  public static final String MSG_ERR = "ERROR: Valor introducido incorrecto";

  public static final int MAX = 16;
  public static final int MIN = 8;

  public final void launchApp() {

    //Mail de PAON
    int longPass01 = UtilesPassword.RND.nextInt(MAX - MIN + 1) + MIN;
//    int longPass74 = UtilesPassword.RND.nextInt(MAX - MIN + 1) + MIN;
//
    char[] pass01 = new char[longPass01];
//    char[] pass74 = new char[longPass74];
//
//    UtilesPassword.generarPassword(pass74);
//
    System.out.printf("Muestra password generado en clase: ");
    System.out.println("No está vinculado al ejercicio...");
    UtilesPassword.mostrarPassword(UtilesPassword.generarCharPassword(pass01));
//    System.out.println("---");
//    UtilesPassword.mostrarPassword(pass74);

    System.out.println();
    System.out.println();
//        SIGUEEE
    //Instancia de un log y la lista vacía de credenciales.
    ListaLog listaLog = new ListaLog();
    ListaCreden listaCreden = new ListaCreden();

    //Creamos un objeto ejemplo y lo añadimos a la lista de credenciales.
    //Este primer user tiene un pass que no va a ser válido. El constructor
    //le asignará uno generado aleatoriamente, sí, en formato String.........
    //
// Insert users de relleno:
    User u2 = new User("J0n$2020", "mer");
    //añadimos un segundo user para comprobar que sí que acepta el pass en debug.
    User u3 = new User("J0n$2121", "J0n$2121");
    listaCreden.addUser(u2);
    listaCreden.addUser(u3);

    //un new Login(u)
    User u;
    Login login;
    Logout logout;
    Rol r;
    //Ordenar código...............
    String user;
    String pass;
    String rol;

    int opcion;  //menú principal
    int intentos = 3;
    //boolean de la vida, el que verifica cosas.
    boolean isOk;
    // cond de operación
    boolean ok;

    do {
      //primero debería loguearse, una vez dentro el menu...
      //no se valida nada para que, por si el usuario se equivoca, ésto cuente
      //como un intento fallido de meter la contraseña, reflejado con
      // int intentos = 0 y este bucle do while que empieza.
      user = UtilesEntrada.leerTexto(MSG_USER);
      if (user.equals("0")) {
        intentos = 0;
      } else {
        pass = UtilesEntrada.leerTexto(MSG_PASS);

        //si al validar el formato del user y el pass....
        //Desarrollar...
        if (UtilesValidacion.validar(user, User.REG_USER)
                && UtilesValidacion.validar(pass, User.REG_PASS)) {

          //Crea un usuario para loguear el login, en este caso es Rol.ADMIN
          //     porque sí.
          //Si la validación de USER Y PASS ES OK: 
          //crea un nuevo login y lo añade al Log
          //luego pasa al menú que inicialmente no hace nada.
          if (listaCreden.checkPass(listaCreden.buscaUser(user), pass)) {
            //Instancia del main del menú Artículos, supongo que lo normal será moverlo 
//y que se ejecute si el user hace login.
            AppItems appItems = new AppItems();
            //Si conecta reseteamos el contador intentos de la sesión.
            intentos = 3;
            //Habría que mirar aquí para hacer dos sesiones dependiendo el rol
            //Con if /else o probar switch para dif roles?
            User sessionUser = new User(user, pass, Rol.ADMIN);
            login = new Login(sessionUser);
            listaLog.addLogIn(login);
            //muestra info login, acordarse del logout!
            login.muestraInfoLogin();

            //Si hace el login pasa al menú principal
            do {
              muestraBanner();
              opcion = UtilesEntrada.leerEntero(MSG_OP, MSG_ERR);
              switch (opcion) {
//CASE 1 -     Añadir user.
                case 1:
                  isOk = false;
                  //validamos nombre de usuario
                  do {
                    user = UtilesEntrada.leerTexto(MSG_NEW_USER);
                    //Añadir condición si el user ya está creado
                    if (!UtilesValidacion.validar(user, User.REG_USER)) {
                      System.out.println("ERROR: Formato de nombre incorrecto");
                    } else if (listaCreden.buscaUser(user) != null) {
                      System.out.println("Elemento ya añadido");
                      isOk = true;
                    } else {
                      isOk = true;
                    }
                  } while (!isOk);

                  //si encuentra el user...
                  if (listaCreden.buscaUser(user) == null) {
                    //reset de isOk para controlar el password
                    isOk = false;
                    //validamos pass
                    do {
                      pass = UtilesEntrada.leerTexto(MSG_NEW_PASS);
                      if (!UtilesValidacion.validar(pass, User.REG_PASS)) {
                        System.out.println("ERROR: Formato de password incorrecto");
                      } else {
                        isOk = true;
                      }
                    } while (!isOk);

                    //una vez validados user y pass, pasamos a elegir rol.
                    do {
                      r = null;
                      rol = UtilesEntrada.leerTexto(MSG_NEW_ROL).toLowerCase();
                      switch (rol) {
                        case "user":
                          r = Rol.USER;
                          break;
                        case "admin":
                          r = Rol.ADMIN;
                          break;
                        default:
                          System.out.println("ERROR: Rol introducido incorrecto.");
                      }
                    } while (r == null);
                    u = new User(user, pass, r);
                    ok = listaCreden.addUser(u);

                    if (ok) {
                      System.out.printf("%nUser añadido%n%n");
                    } else {
                      System.out.printf("%nERROR: Operación NO completada.%n%n");
                    }
                  }
                  break;
                case 2:
//Case 2    --- Muestra Lista Credenciales
                  if (listaCreden.numUsers() > 0) {
                    listaCreden.mostrarUsers();
                  }
                  break;
                case 3:
//Case 3  --------Debería saltar al menú Items...
                  appItems.launchApp();
                  break;
//Case 0     -- SALIR               
                case 0:
                  logout = new Logout(sessionUser);
                  //capturamos evento logout aquí, por ejemplo.
                  listaLog.addLogOut(logout);
                  //muestra info login, acordarse del logout!
                  logout.muestraInfoLogout();
                  break;
                default:
                  throw new AssertionError();
              }
            } while (opcion != 0);
          }//fin del if si loguea
          else {
            //Si al comprobar las credenciales devuelve false...
            muestrIntentos(intentos);
            intentos--;
          }
        } else { //Si alguno de los formatos no es correcto
          muestrIntentos(intentos);
          intentos--;
        }

      } //Cierre del else, si el user != 0;
      /* 
 TODO:
 Si creamos un user solo con 
 Como el user introducido "Paco" "hay que validar el pass" 
      no cumple con las reglas del password
 podríamos generarle directamente en el constructor uno aleatorio y mostrarlo por 
 pantalla, a lo bastidor.
       */
      //falla en esta línea, lleva al equals de comparar USERS, COGERLO DE POR AHÍ!
      //EN EL IF: Hacer una búsqueda en listaCreden y comparar con los datos
      //de user y pass de ENTRADA.
    } while (intentos > 0 && !user.equals("0"));
    System.out.printf("%nMENSAJE DE SALIDA%n"
            + "----%n"
            + "Gracias por utilizar este programa.%n");
  }

  public static final void muestraBanner() {
    System.out.println();
    System.out.println("SISTEMA DE LOGIN");
    System.out.println();
    System.out.println(" Menú principal");
    System.out.println("----------------");
    System.out.println("1.- Añadir User");
    System.out.println("2.- Muestra Lista de Credenciales");
    System.out.println("3.- Entrada al menú items.");
    System.out.println();
    System.out.println("---");
    System.out.println("0.- Salir");
  }

  public static final void muestrIntentos(int intentos) {
    //Si al comprobar las credenciales devuelve false...
    intentos--;
    switch (intentos) {
      case 2:
        System.out.println("ERROR: Usuario o password incorrectos.");
        System.out.printf("Te quedan %d intentos%n%n", intentos);
        break;
      case 1:
        System.out.println("ERROR: Usuario o password incorrectos.");
        System.out.printf("Te queda %d intento%n%n", intentos);
        break;
      case 0:
        System.out.println("---");
    }

  }
}
