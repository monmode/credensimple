/*
 * Copyright (C) 2020 CicloM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.monmo.java.libraries;

import java.util.Random;

/**
 *
 * @author CicloM
 */
public class UtilesPassword {

  public static final String MAY = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
  public static final String MIN = "abcdefghijklmnñopqrstuvwxyz";
  public static final String DIG = "0123456789";
  public static final String PUN = "@#!_";
  public static final String TOT = MAY + MIN + DIG + PUN;

  // Sistema Números Aleatórios
  public static final Random RND = new Random();

  public static final void generarPassword(char[] pass) {
    pass[0] = MAY.charAt(RND.nextInt(MAY.length() - 1));
    pass[1] = MIN.charAt(RND.nextInt(MIN.length() - 1));
    pass[2] = DIG.charAt(RND.nextInt(DIG.length() - 1));
    pass[3] = PUN.charAt(RND.nextInt(PUN.length() - 1));
    
    //generamos los otros valores del array
    for (int i = 4; i < pass.length; i++) {
      pass[i] = TOT.charAt(RND.nextInt(TOT.length() - 1));
    }
    UtilesArrays.desordenar(pass);
  }
  
  public static final String generarStringPassword(int longitud) {
    char[] pass = new char[longitud];
    
    StringBuilder sb = new StringBuilder();
    pass[0] = MAY.charAt(RND.nextInt(MAY.length() - 1));
    pass[1] = MIN.charAt(RND.nextInt(MIN.length() - 1));
    pass[2] = DIG.charAt(RND.nextInt(DIG.length() - 1));
    pass[3] = PUN.charAt(RND.nextInt(PUN.length() - 1));

    sb.append(pass[0]);
    sb.append(pass[1]);
    sb.append(pass[2]);
    sb.append(pass[3]);
    
    //generamos los otros valores del array
    for (int i = 4; i < pass.length; i++) {
      pass[i] = TOT.charAt(RND.nextInt(TOT.length() - 1));
      sb.append(pass[i]);
    }
    UtilesArrays.desordenar(pass);
    return sb.toString();
  }
  
  

  public static final char[] generarCharPassword(char[] pass) {
    pass[0] = MAY.charAt(RND.nextInt(MAY.length() - 1));
    pass[1] = MIN.charAt(RND.nextInt(MIN.length() - 1));
    pass[2] = DIG.charAt(RND.nextInt(DIG.length() - 1));
    pass[3] = PUN.charAt(RND.nextInt(PUN.length() - 1));

    //generamos los otros valores del array
    for (int i = 4; i < pass.length; i++) {
      pass[i] = TOT.charAt(RND.nextInt(TOT.length() - 1));
    }
    UtilesArrays.desordenar(pass);

    return pass;
  }

  public static final void mostrarPassword(char[] pass) {
    for (int i = 0; i < pass.length; i++) {
      System.out.printf("%c", pass[i]);
    }
    System.out.println();
  }
}
