/*
 * Copyright (C) 2020 CicloM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.monmo.java.libraries;

import static org.monmo.java.libraries.UtilesPassword.RND;

/**
 *
 * @author CicloM
 */
public class UtilesArrays {
  // Desordenar Lista - caracter

  public static final void desordenar(char[] lista) {
    for (int i = 0; i < lista.length; i++) {
      int posRnd = RND.nextInt(lista.length);

      char aux = lista[posRnd];
      lista[posRnd] = lista[i];
      lista[i] = aux;
    }

  }
}
