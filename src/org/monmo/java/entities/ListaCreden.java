/* 
 * Copyright (C) 2020 mon_mode   0mon.mode@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.monmo.java.entities;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Jonsui
 */
public class ListaCreden {

  private ArrayList<User> lista = new ArrayList<>();

  public ListaCreden() {
    lista = new ArrayList<>();
  }

  //Qué sentido tendría aquí crear el parametrizado?
  //
  //sigue...
  public boolean addUser(User u) {
    boolean isOk = false;
    if (u != null) {
      isOk = lista.add(u);
    }
    return isOk;
  }

  public boolean delUser(User u) {
    boolean isOk = false;
    if (u != null) {
      isOk = lista.remove(u);
    }
    return isOk;
  }

  public boolean delUserFound(String user) {
    boolean del = false;
    User us;
    us = buscaUser(user);
    if (us != null) {
      del = lista.remove(us);
    }
    return del;
  }

//método que busca usuario por su nombre de usuario. Valga la rebuznancia.
  public User buscaUser(String u) {
    User us;
    User enc = null;
    boolean exit = false;
    Iterator<User> it = lista.iterator();

    while (exit == false && it.hasNext()) {
      us = it.next();
      if (us.getUser().equals(u)) {
        exit = true;
        enc = us;
      }
    }
    return enc;
  }

  public boolean checkPass(User u, String pass) {
    boolean isOk = false;
    
    if(u instanceof User){
      isOk = true;
    }
    
    return isOk;
  }
//método que busca usuario por su nombre de usuario. Valga la rebuznancia.

  
  public void mostrarUsers(){
    lista.forEach((u) -> {
      System.out.println();
      u.muestraInfoUser();
    });
  }

  public int numUsers() {
    return lista.size();
  }
}
