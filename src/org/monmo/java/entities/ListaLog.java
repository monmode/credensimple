/* 
 * Copyright (C) 2020 mon_mode   0mon.mode@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.monmo.java.entities;

import java.util.ArrayList;

/**
 *
 * @author Jonsui
 */
public class ListaLog {

  private final ArrayList<Login> logIn;
  private final ArrayList<Logout> logOut;

  public ListaLog() {
    logIn = new ArrayList<>();
    logOut = new ArrayList<>();
  }

  public ListaLog(ArrayList<Login> logIn, ArrayList<Logout> logOut) {
    this.logIn = logIn;
    this.logOut = logOut;
  }

  public boolean addLogIn(Login logIn) {
    boolean isOk = false;
    if (logIn != null) {
      isOk = this.logIn.add(logIn);
    }
    return isOk;
  }

  public boolean addLogOut(Logout logOut) {
    boolean isOk = false;
    if (logOut != null) {
      isOk = this.logOut.add(logOut);
    }
    return isOk;
  }

}
