/* 
 * Copyright (C) 2020 mon_mode   0mon.mode@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.monmo.java.entities;

import java.util.Date;

/**
 *
 * @author Jonsui
 */
public class Login {

  public static final Date DEF_DATE = new Date(System.currentTimeMillis());
  
  private User user;
  private Date date;

  public Login() {
  }

  public Login(User user) {
    this.user = user;
    this.date = DEF_DATE;
  }

  public User getUser() {
    return user;
  }

  public Date getDate() {
    return date;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  @Override
  public String toString() {
    return String.format("-----------------------------------------------------%n"
            + "INFO DEL LOGIN%n"
            + "NOMBRE DE USUARIO: %s%n"
            + "FECHA DE LOGUEO..: %s %n"
            + "-----------------------------------------------------%n", 
            user.getUser(), date);
  }
  
  public void muestraInfoLogin(){
    System.out.println(toString());
  }
  
//
//  public boolean validar() {
//    boolean isOk = false;
//    String u;
//    do {
//      u = UtilesEntrada.leerTexto(MSG_USER);
//      if (!u.matches(REG_USER)) {
//        System.out.println("ERROR: Nombre de usuario inválido.");
//      }
//    } while (!u.matches(REG_USER));
//    //Sigue
//    String pass = UtilesEntrada.leerTexto(MSG_PASS);
//    if (pass != null && pass.matches(REG_PASS)) {
//      //Si el password no es null y es aceptado por la regexp.
//      //Falta comprobar en lista creden que están en la lista.
//      isOk = true;
//    }
//
//    return isOk;
//  }
}
