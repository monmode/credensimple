/* 
 * Copyright (C) 2020 mon_mode   0mon.mode@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.monmo.java.entities;

import java.io.Serializable;
import java.util.Objects;
import org.monmo.java.libraries.UtilesPassword;
import org.monmo.java.libraries.UtilesValidacion;

/**
 *
 * @author Jonsui
 */
public class User implements Serializable {

  private static final long serialVersionUID = 1130200487264377334L;

  public static final String DEF_USER = "DEF_USER";
  public static final String DEF_PASS = "";
  public static final String MSG_USER = "Introduzca el usuario: ";
  public static final String MSG_PASS = "Introduzca el password: ";
  public static final int LONG_MIN_USER = 6;
  public static final int LONG_MIN_PASS = 8;
  public static final int LONG_MAX_USER = 12;
  public static final int LONG_MAX_PASS = 18;
  public static final char AMPER = '&';
  public static final String REG_USER = String.format(
          "((?=.*[a-z])(?=.*[0-9])(?=.*[_@#$~%%&])(?=.*[A-Z]).{%d,%d})",
          LONG_MIN_USER, LONG_MAX_USER);
  public static final String REG_PASS = String.format(
          "((?=.*[a-z])(?=.*[0-9])(?=.*[_@#$~%%&])(?=.*[A-Z]).{%d,%d})",
          LONG_MIN_PASS, LONG_MAX_PASS);

  public static final int MAX = 9;
  public static final int MIN = 6;
  public static final int DEF_LONG = UtilesPassword.RND.nextInt(MAX - MIN + 1) + MIN;

  private String user;
  private String pass;
  private Rol rol;

  public User() {
    user = DEF_USER;
    pass = DEF_PASS;
    rol = Rol.DEF_ADMIN;
  }

  public User(String user, String pass) {
    if (UtilesValidacion.validar(user, REG_USER)) {
      this.user = user;
    } else {
      this.user = DEF_USER;
    }
    if (UtilesValidacion.validar(pass, REG_PASS)) {
      this.pass = pass;
    } else {
      this.pass = UtilesPassword.generarStringPassword(DEF_LONG);
    }
    this.rol = Rol.DEF_ADMIN;
  }

  public User(String user, String pass, Rol rol) {
    if (UtilesValidacion.validar(user, REG_USER)) {
      this.user = user;
    } else {
      this.user = DEF_USER;
    }
    this.pass = pass;
    this.rol = rol;
  }
//Expresamente que al crear el User por este constructor lo
  //genere con un Rol.USER;

  public User(String user) {
    if (UtilesValidacion.validar(user, REG_USER)) {
      this.user = user;
    } else {
      this.user = DEF_USER;
    }
    this.rol = Rol.USER;
  }

  public String getUser() {
    return user;
  }

  public String getPass() {
    return pass;
  }

  public Rol getRol() {
    return rol;
  }

  public void setRol(Rol rol) {
    this.rol = rol;
  }

  public void setUser(String user) {
    if (UtilesValidacion.validar(user, REG_USER)) {
      this.user = user;
    }
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  @Override
  public boolean equals(Object o) {
    boolean testOk;
    if (o == null) {
      testOk = false;
    } else if (!(o instanceof User)) {
      testOk = false;
    } else {
      testOk = user.equals(((User) o).getUser())
              && pass.equals(((User) o).getPass())
              && rol == ((User) o).getRol();
    }
    return testOk;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 97 * hash + Objects.hashCode(this.user);
    hash = 97 * hash + Objects.hashCode(this.pass);
    hash = 97 * hash + Objects.hashCode(this.rol);
    return hash;
  }

  @Override
  public String toString() {
    return String.format("INFO DEL USUARIO%n"
            + "----------------%n"
            + "NOMBRE ..: %s%n"
            + "PASSWORD : %s%n"
            + "ROL .....: %s%n"
            + "---", user, pass, rol);
  }

  public void muestraInfoUser() {
    System.out.println(toString());
  }

}
